<?php

$fname = "ім'я";
$lname = "прізвище";
$email = "example@gmail.com";
$pass = "123456789";
$passrep = "123456789";
$sex = "men";
$birthday = "2002";
$country = "Україна";
$autemail = "example@gmail.com";
$autpass = "123456789";
$leng = [
    "Азербайджанська",
    "Непальська",
    "Aлбанська",
    "Нідерландська",
    "Англійська",
    "Німецька",
    "Арабська",
    "Норвезька",
    "Білоруська",
    "Польська",
    "Болгарська",
    "Португальська",
    "В’єтнамська",
    "Російська",
    "Вірменська",
    "Румунська",
    "Голландськ",
    "Сербська",
    "Грецька",
    "Словацька",
    'Українська'
];

$heloleng = ['ru' => 'Привет', 'en' => 'Hello', 'ua' => 'Привіт', 'fr' => 'Hé', 'de' => "Hallo"];
$users = [
    '1' => [
        "fname" => "Тест",
        'lname' => 'Тест',
        "email" => "test@test.com",
        'pass' => 'test',
        'leng' => 'Українська',
        'sex' => 'men',
        'birthday' => 2000,
        'country' => 'Test',
        'lang' => 'ua'
    ],
    '2' => [
        "fname" => "Антон",
        'lname' => 'Антонов',
        "email" => "антон@test.com",
        'pass' => 'антон',
        'leng' => 'Українська',
        'sex' => 'men',
        'birthday' => 2009,
        'country' => 'Україна',
        'lang' => 'ua'
    ],
    '3' => [
        "fname" => "Виталя",
        'lname' => 'Витальев',
        "email" => "виталя@test.com",
        'pass' => 'Виталя',
        'leng' => 'Українська',
        'sex' => 'men',
        'birthday' => 2001,
        'country' => 'Україна',
        'lang' => 'ua'
    ],
    '4' => [
        "fname" => "Ульяна",
        'lname' => 'Ульянівна',
        "email" => "ульяна@test.com",
        'pass' => 'Ульяна',
        'leng' => 'Українська',
        'sex' => 'women',
        'birthday' => 1998,
        'country' => 'Україна',
        'lang' => 'ua'
    ],
    '5' => [
        "fname" => "Діана",
        'lname' => 'Діанівна',
        "email" => "діана@test.com",
        'pass' => 'Діана',
        'leng' => 'Українська',
        'sex' => 'women',
        'birthday' => 2000,
        'country' => 'Китай',
        'lang' => 'fr'
    ],
    '6' => [
        "fname" => "Діана",
        'lname' => 'Діанівна',
        "email" => "діана@test.com",
        'pass' => 'Діана',
        'leng' => 'Українська',
        'sex' => 'women',
        'birthday' => 2000,
        'country' => 'Китай',
        'lang' => 'fr'
    ],
    '7' => [
        "fname" => "Діана",
        'lname' => 'Діанівна',
        "email" => "діана@test.com",
        'pass' => 'Діана',
        'leng' => 'Українська',
        'sex' => 'women',
        'birthday' => 2000,
        'country' => 'Китай',
        'lang' => 'fr'
    ],
    '8' => [
        "fname" => "Ульяна",
        'lname' => 'Ульянівна',
        "email" => "ульяна@test.com",
        'pass' => 'Ульяна',
        'leng' => 'Українська',
        'sex' => 'women',
        'birthday' => 1998,
        'country' => 'Україна',
        'lang' => 'ua'
    ],
];


$date = '31-12-2020';
$mystring='.';
$buf=[];
$buf=str_getcsv($date,'-');
for($i=2;$i>-1;$i--){
    $mystring.=$buf[$i];
    $mystring.=".";
}
$mystring=trim($mystring,'.');
echo $mystring;
echo "</br>";


$string = 'london is the capital of great britain';
$buf=str_getcsv($string,' ');
$mystring=".";
for($i=0;$i<count($buf);$i++){
$mystring.=ucfirst($buf[$i]);
    $mystring.=" ";
}
$mystring=trim($mystring,'.');
$mystring=trim($mystring,' ');
echo $mystring;
echo "</br>";

$password="12345678";
if (iconv_strlen($password) < 12 and iconv_strlen($password) > 7) {
    echo "Пароль підходить";
    echo "</br>";
}else{
    echo "Придумайте новий пароль";
    echo "</br>";
}


$value = 'jfFfhf55555kdjd89';
echo preg_replace("/[0-9]/", '', $value);
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Test</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN"
            crossorigin="anonymous"></script>
</head>
<body>
<div class="container text-center">
    <div class="row">
        <div class="col">
            <h3>Реєстрація</h3>
            <form method="post">
                <div>
                    <label class="form-label" for="fname">Ім'я:</label><br>
                    <input class="form-control" type="text" id="fname" name="fname" value="<?php
                    echo $fname ?>"
                           required>
                </div>
                <div>
                    <label class="form-label" for="lname">Прізвище:</label><br>
                    <input class="form-control" type="text" id="lname" name="lname" value="<?php
                    echo $lname ?>"
                           required>
                </div>
                <div>
                    <label class="form-label" for="email">Email:</label><br>
                    <input class="form-control" type="email" id="email" name="email" value="<?php
                    echo $email ?>"
                           required>
                </div>
                <div>
                    <label class="form-label" for="pass">Пароль:</label><br>
                    <input class="form-control" type="password" id="pass" name="pass" value="<?php
                    echo $pass ?>"
                           required>
                </div>
                <div>
                    <label class="form-label" for="passrep">Повторіть пароль:</label><br>
                    <input class="form-control" type="password" id="passrep" name="passrep"
                           value="<?php
                           echo $passrep ?>" required>
                </div>
                <div>
                    <label class="form-label" for="lang">Країна:</label>
                    <select class="form-select" name="lang" required>
                        <option value=""></option>
                        <?php
                        foreach ($leng as $key => $value):
                            echo '<option value="' . $key . '">' . $value . '</option>'; //close your tags!!
                        endforeach;
                        ?>
                    </select>
                </div>
                <div class="form-check">
                    Стать: <br>
                    <input class="form-check-input" type="radio" id="men" name="sex" value="men" required>
                    <label class="form-check-label" for="men">Чоловік</label><br>
                    <input class="form-check-input" type="radio" id="wonen" name="sex" value="wonen" required>
                    <label class="form-check-label" for="wonen">Жінка</label><br>
                </div>
                <div>
                    <label class="form-label" for="birthday">Рік народження:</label><br>
                    <input class="form-control" type="text" id="birthday" name="birthday"
                           value="<?php
                           echo $birthday ?>" required>
                </div>
                <div>
                    <label class="form-label" for="country">Країна:</label><br>
                    <input class="form-control" type="text" id="country" name="country" value="<?php
                    echo $country ?>"
                           required>
                </div>
                <div>
                    <input class="btn btn-primary" type="submit" name="add" value="Зареєструватися" required>
                </div>
            </form>
        </div>
        <div class="col">
            <form method="post">
                <h3>Авторизація</h3>
                <div>
                    <label class="form-label" for="autemail">Email:</label><br>
                    <input class="form-control" type="email" id="autemail" name="autemail"
                           value="<?php
                           echo $autemail ?>" required>
                </div>
                <div>
                    <label class="form-label" for="autpass">Пароль:</label><br>
                    <input class="form-control" type="password" id="autpass" name="autpass"
                           value="<?php
                           echo $autpass ?>" required>
                </div>
                <div>
                    <input class="btn btn-primary" type="submit" name="add" value="Увійти" required>
                </div>
            </form>
        </div>
    </div>
</div>

</body>
</html>